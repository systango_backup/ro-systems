class ChangeDataTypeForCustomersContactNumbers < ActiveRecord::Migration
  def self.up
    change_table :customers do |t|
      t.change :mobile_number, :string
      t.change :phone_number, :string
    end
  end

  def self.down
    change_table :customers do |t|
      t.change :mobile_number, :decimal
      t.change :phone_number, :decimal
     
    end
  end
end
