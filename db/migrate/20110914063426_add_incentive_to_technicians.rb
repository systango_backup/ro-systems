class AddIncentiveToTechnicians < ActiveRecord::Migration
  def self.up
    add_column :technicians, :incentive, :integer
  end

  def self.down
    remove_column :technicians, :incentive
  end
end
