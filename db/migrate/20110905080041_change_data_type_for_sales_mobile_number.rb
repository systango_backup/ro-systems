class ChangeDataTypeForSalesMobileNumber < ActiveRecord::Migration
  def self.up
    change_table :sales do |t|
      t.change :mobile_number, :string
      
    end
  end

  def self.down
     change_table :sales do |t|
      t.change :mobile_number, :decimal
     
    end
  end
end
