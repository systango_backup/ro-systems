class AddCompanyIdToSales < ActiveRecord::Migration
  def self.up
    add_column :sales, :company_id, :integer
  end

  def self.down
    remove_column :sales, :company_id
  end
end
