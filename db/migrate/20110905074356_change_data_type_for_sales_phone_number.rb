class ChangeDataTypeForSalesPhoneNumber < ActiveRecord::Migration
  def self.up
    change_table :sales do |t|
      t.change :phone_number, :string
      
    end
  end

  def self.down
    change_table :sales do |t|
      t.change :phone_number, :decimal
     
    end
  end
end
