class ChangeUpdateForUserTypes < ActiveRecord::Migration
  def self.up
    change_table :user_type do |t|
      UserType.create :name => "Service only"
      UserType.create :name => "AMC only"
      UserType.create :name => "Sale only"
      UserType.create :name => "Service, Sale and AMC"
      end
  end

  def self.down
  end
end
