class ChangeDataTypeForOrdersContactNumbers < ActiveRecord::Migration
  def self.up
    change_table :orders do |t|
      t.change :mobile_number, :string
      t.change :phone_number, :string
    end
  end

  def self.down
    change_table :orders do |t|
      t.change :mobile_number, :decimal
      t.change :phone_number, :decimal
     
    end
  end
end
