class AddRecdAmountFromOrderPayments < ActiveRecord::Migration
  def self.up
    add_column :order_payments, :recd_amount, :float
  end

  def self.down
    remove_column :order_payments, :recd_amount
  end
end
