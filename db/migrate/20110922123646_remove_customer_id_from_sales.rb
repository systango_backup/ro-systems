class RemoveCustomerIdFromSales < ActiveRecord::Migration
  def self.up
    remove_column :sales, :customer_id
  end

  def self.down
    add_column :sales, :customer_id, :integer
  end
end
