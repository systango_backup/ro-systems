class AddCreatedByToSales < ActiveRecord::Migration
  def self.up
    add_column :sales, :created_by, :integer
  end

  def self.down
    remove_column :sales, :created_by
  end
end
