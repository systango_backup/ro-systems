class CreateAssemblies < ActiveRecord::Migration
  def self.up
    create_table :assemblies do |t|
      t.string :name
      t.string :description
      t.integer :quantity
      t.float :total_amount

      t.timestamps
    end
  end

  def self.down
    drop_table :assemblies
  end
end
