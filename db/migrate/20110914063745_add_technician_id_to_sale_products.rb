class AddTechnicianIdToSaleProducts < ActiveRecord::Migration
  def self.up
    add_column :sale_products, :technician_id, :integer
  end

  def self.down
    remove_column :sale_products, :technician_id
  end
end
