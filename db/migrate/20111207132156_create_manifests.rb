class CreateManifests < ActiveRecord::Migration
  def self.up
    create_table :manifests do |t|
      t.integer :product_id
      t.integer :assembly_id

      t.timestamps
    end
  end

  def self.down
    drop_table :manifests
  end
end
