class AddProductIdToSales < ActiveRecord::Migration
  def self.up
    add_column :sales, :product_id, :integer
  end

  def self.down
    remove_column :sales, :product_id
  end
end
