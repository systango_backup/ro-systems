class AddCreatedByToCustomers < ActiveRecord::Migration
  def self.up
    add_column :customers, :created_by, :integer
  end

  def self.down
    remove_column :customers, :created_by
  end
end
