class AddAmcTypeToSales < ActiveRecord::Migration
  def self.up
    add_column :sales, :amc_type, :string
  end

  def self.down
    remove_column :sales, :amc_type
  end
end
