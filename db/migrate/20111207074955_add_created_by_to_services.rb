class AddCreatedByToServices < ActiveRecord::Migration
  def self.up
    add_column :services, :created_by, :integer
  end

  def self.down
    remove_column :services, :created_by
  end
end
