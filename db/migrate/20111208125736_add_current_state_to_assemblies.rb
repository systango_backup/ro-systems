class AddCurrentStateToAssemblies < ActiveRecord::Migration
  def self.up
    add_column :assemblies, :current_state, :string
  end

  def self.down
    remove_column :assemblies, :current_state
  end
end
