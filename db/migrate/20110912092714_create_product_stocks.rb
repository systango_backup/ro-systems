class CreateProductStocks < ActiveRecord::Migration
  def self.up
    create_table :product_stocks do |t|
      t.integer :product_id
      t.integer :product_serial_number_id
      t.integer :technician_id
      t.integer :stock_quantity

      t.timestamps
    end
  end

  def self.down
    drop_table :product_stocks
  end
end
