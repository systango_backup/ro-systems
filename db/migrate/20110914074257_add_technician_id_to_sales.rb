class AddTechnicianIdToSales < ActiveRecord::Migration
  def self.up
    add_column :sales, :technician_id, :integer
  end

  def self.down
    remove_column :sales, :technician_id
  end
end
