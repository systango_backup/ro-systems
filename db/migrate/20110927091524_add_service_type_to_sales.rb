class AddServiceTypeToSales < ActiveRecord::Migration
  def self.up
    add_column :sales, :service_type, :string
  end

  def self.down
    remove_column :sales, :service_type
  end
end
