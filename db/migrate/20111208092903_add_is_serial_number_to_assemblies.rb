class AddIsSerialNumberToAssemblies < ActiveRecord::Migration
  def self.up
    add_column :assemblies, :is_serial_number, :boolean
  end

  def self.down
    remove_column :assemblies, :is_serial_number
  end
end
