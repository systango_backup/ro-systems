class ProductsController < ApplicationController
  protect_from_forgery :except => [:destroy]
### Check Authendication before calling any function
  before_filter :authenticate
  before_filter :option_access, :only => [ :user_stock_transfer, :user_stock]
  before_filter :is_admin, :except => [ :user_stock_transfer, :user_stock]

### Open Create Product Form
  def new_product
    @product = Product.new
  end

### Save Product Information to Database
  def create_product

    @product = Product.new(params[:product])
    if @product.save
      redirect_to product_stock_path
    else
      flash[:notice] = "Please enter correct Informations"
      render product_new_path
    end
  end

### Open Edit Product Form
  def edit_product
    begin
      @product = Product.find(params[:id])
    rescue
    end
  end

### Update Product Information to Database
  def update_product
    begin
      @product = Product.find(params[:id])
      if @product.update_attributes(params[:product])
        redirect_to product_stock_path
      else
        flash[:notice] = "Please enter correct Informations"
        render product_edit_path
      end
    rescue
    end
  end

### Delete Product Information from Database
  def destroy
    begin
      @product = Product.find(params[:id])
      @product.update_attribute(:is_enable, false)
    rescue
    end
    redirect_to product_stock_path
  end

### Get Stock details In Particular order
  def ordered_stock_detail
    stocks = Product.get_stock_detail(params)
    render_partial_pagination("stock_detail", stocks, params[:page], :stocks)
  end

  def user_stock_transfer
  end

### Save transfer Stock 
  def save_stock_transfer
    if params[:product].blank?
      flash[:notice] = "Please select product"
      render :action=>:user_stock_transfer
    else
      @product_stock1 =ProductStock.find(:first, :conditions => ["technician_id = ? and product_id = ?", params[:stock][:technician_id], params[:product][:name]])
      if !@product_stock1.nil?
        update_quantity = @product_stock1.stock_quantity - Integer(params[:stock][:stock_quantity])
        if update_quantity < 0
          flash[:notice] = "Can't transfer product"
          render :action=>:user_stock_transfer
        else 
          @product_stock1.update_attributes(:stock_quantity => update_quantity)
          @product_stock2 = ProductStock.find(:first, :conditions => ["technician_id = ? and product_id = ?", params[:stock1][:technician_id], params[:product][:name]])
          if @product_stock2.nil?
             @product_stock_3 = ProductStock.new(:product_id => params[:product][:name],:stock_quantity => params[:stock][:stock_quantity],:technician_id => params[:stock1][:technician_id])
             @product_stock_3.save
          else
            update_quantity11 = @product_stock2.stock_quantity + Integer(params[:stock][:stock_quantity])
            @product_stock2.update_attributes(:stock_quantity=>update_quantity11)
          end
          flash[:notice] = "Transfer Product Successfully"
          redirect_to :action=>:user_stock
        end
      end
    end
  end

  def test
    unless params[:p_id].blank? && params[:stock].blank?
      @quantity = ProductStock.find(:first, :conditions => ["product_id = ? and technician_id = ?", params[:p_id], params[:stock]])
    end
    render :json => @quantity.stock_quantity.to_json
  end

  def product_stock_row
    @product_stock = ProductStock.find(:all,:conditions=>["technician_id= ? ",params[:from_id]])
    @product_stock.each do |stock|
      product = Product.find(stock.product_id)
      stock[:name] = product.name
      stock[:p_id] = product.id
    end
  end

  def user_stock_transfer_from
    @product_stock = ProductStock.find(:all,:conditions=>["technician_id= ? ",params[:technician_id]])
    render :partial => "products/user_stock_detail", :locals => { :stocks => @product_stock, :counter => 0, :total_pages => 0}, :layout=>false
  end

### Get Stock transfer details for all users
  def stock_transfer_details
    @product_stock = ProductStock.all
  end

### Stock transfer from one user to anthor 
  def user_transfer
    @product_stock = ProductStock.find(:all,:conditions=>["technician_id= ? ",params[:technician_id]])
    render :partial => "products/user_stock_detail", :locals => { :stocks => @product_stock, :counter => 0, :total_pages => 0}, :layout=>false
  end

### Get Stock transfer details In Particular user
  def save_user_stock
#      if params[:product].blank?
#        flash[:notice] = "Please select product"
#        render :action=>:stock_transfer
#      else
    if params[:stock][:user_type] == "1"
      params[:stock][:stock_product].each do | stock_product_key,stock_product_val |
        @product_stock = ProductStock.new(:product_id => stock_product_val[:product_id],:stock_quantity => stock_product_val[:stock_quantity],:technician_id => params[:stock][:technician_id],:product_serial_number_id => params[:stock][:product_serial_number_id])
        @product =Product.find(:first, :conditions => ["id = ?",@product_stock.product.id])
        if !@product.blank?
          update_quantity = @product.current_stock - stock_product_val[:stock_quantity].to_i
          if update_quantity < 0
            flash[:notice] = "Product is not available for stock"
            #render :action=>:service_form
            break
          else 
            @product.update_attributes(:current_stock => update_quantity)
            @product_stock1 = ProductStock.find(:first, :conditions => ["technician_id = ? and product_id = ?", params[:stock][:technician_id], stock_product_val[:product_id]])
            if !@product_stock1.blank?
              update_quantity1 = @product_stock1.stock_quantity + stock_product_val[:stock_quantity].to_i
              @product_stock1.update_attributes(:stock_quantity => update_quantity1)
            else 
              @product_stock.save
            end
              flash[:notice] = "Product transfer Successfully"
            #redirect_to customer_complaint_register_path
          end
        end	
# 	unless @product_stock.save
#           flash[:notice] = "some thing went wrong"
#           break
# 	end
      end

    else  
      params[:stock][:stock_product].each do | stock_product_key,stock_product_val |
        @product_stock = ProductStock.find(:first, :conditions => ["technician_id = ? and product_id = ?", params[:stock][:technician_id], stock_product_val[:product_id]])
        if !@product_stock.blank?
          update_quantity = @product_stock.stock_quantity - stock_product_val[:stock_quantity].to_i
          if update_quantity < 0
            flash[:notice] = "Product is not available for stock"
            #render :action=>:service_form
            break
          else
            @product_stock.update_attributes(:stock_quantity => update_quantity)
            @product =Product.find(:first, :conditions => ["id = ?",@product_stock.product.id])
            if !@product.blank?
              update_quantity = @product.current_stock + stock_product_val[:stock_quantity].to_i
              @product.update_attributes(:current_stock => update_quantity)
              flash[:notice] = "Product transfer Successfully"
            end
          end
        end
      end
    end	
    if @product_stock.blank?
      flash[:notice] = "Product is not available for stock"
    end
      flash[:notice] = flash[:notice] ||"Saved Successfully"
      redirect_to :action=>:user_stock	
    #end
  end

### Get user Stock details 
  def user_stock
    @product_stock = ProductStock.all
  end

### Get Stock details for search by product name  
  def ordered_stock_transfer
    unless params[:technician_id].blank?
      @product_stock = ProductStock.find(:all, :include => :product,:conditions=>["technician_id= ? AND lower(products.name) like ? AND lower(products.description) like ?",params[:technician_id], '%'+params[:name].downcase+'%', '%'+params[:description].downcase+'%'])
      render :partial => "products/user_stock_detail", :locals => { :stocks => @product_stock, :counter => 0, :total_pages => 0}, :layout=>false
    else
	@product_stock = ProductStock.find(:all, :include => :product,:conditions=>["lower(products.name) like ? AND lower(products.description) like ?", '%'+params[:name].downcase+'%', '%'+params[:description].downcase+'%'])
      render :partial => "products/user_stock_detail", :locals => { :stocks => @product_stock, :counter => 0, :total_pages => 0}, :layout=>false
    end
  end

### Print Stock Details
  def print_stock
    @quantity, @product, @description, @serial = params[:quantities], params[:products], params[:descriptions], params[:serials]
  end
end
