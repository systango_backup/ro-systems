class Sale < ActiveRecord::Base

  has_many :sale_products, :dependent => :destroy
  has_many :payments, :as => :reference, :dependent => :destroy
  belongs_to :technician
  belongs_to :product
  belongs_to :company
  belongs_to :customer
  alias_attribute :name, :customer_name
  alias_attribute :contract_date, :date
  #alias_attribute "name", "customer_name"

  
  
  validates_uniqueness_of :invoice_number, :scope => :bill_book_number, :message => "^This invoice no. has already used"
### Get Sales detailed Information
  def self.get_sale_information( params )

    data = []
    #begin
      sale_info = ""
      order_str =params[:order_by].to_s.gsub("name","customer_name")
      order_str =order_str.to_s.gsub("contract_date","date")
      order_str =order_str.to_s.gsub("customer_customer_name","customer_name")
      if params[:from_date].blank?

        sale_info = Sale.find(:all, :conditions => ["customer_name ilike ? AND colony ilike ? AND landmark ilike ? AND customer_type ilike ?", "#{params[:name]}%", "#{params[:colony]}%", "#{params[:landmark]}%", "%#{params[:customer_type]}"], :order => order_str)
      else
        params[:to_date] = Date.today.strftime('%Y/%m/%d') if params[:to_date].blank?
        sale_info = Sale.find(:all, :conditions => ["customer_name ilike ? AND colony ilike ? AND landmark ilike ? AND customer_type ilike ? AND date BETWEEN ? AND ?", "#{params[:name]}%", "#{params[:colony]}%", "#{params[:landmark]}%", "%#{params[:customer_type]}", Date.parse(params[:from_date]).strftime("%Y/%m/%d"), Date.parse(params[:to_date]).strftime("%Y/%m/%d")], :order => order_str)
      end
      unless sale_info.blank?
        sale_info.each do |sale|

          data_hash = {}
          data_hash["date"] = sale.date.blank? ? Date.today.strftime("%d/%m/%y") : sale.date.strftime("%d/%m/%y")
          data_hash["sale_id"] = sale.id.blank? ? "-": sale.id
          data_hash["customer_name"] = sale.customer_name.blank? ? sale.company.try(:name) : sale.customer_name.capitalize
          data_hash["customer_type"] = sale.customer_type.capitalize
          if sale.company_id.blank?
            data_hash["address"] = Customer.get_address( sale )
          else
            data_hash["address"] = Company.get_company_address( sale.company_id )
          end
          data_hash["contact_numbers"] = Customer.get_contacts( sale )
          data_hash["amount"] = sale.total_amount.blank? ? "-" : sale.total_amount
          data_hash["created_by"] = sale.try(:created_by)#created_by.blank? ? "-" : sale.created_by
          data << data_hash
        end
      end
    #rescue
    #end
    data
  end
 end
