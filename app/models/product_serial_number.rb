class ProductSerialNumber < ActiveRecord::Base

  belongs_to :product
  has_many :sale_products, :as => :product_reference
  has_many :product_stocks

end
