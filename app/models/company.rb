class Company < ActiveRecord::Base

  has_many :orders
  has_many :sales

  validates :name, :presence => true
  validates :city, :presence => true
  validates :state, :presence => true


  ### Get Company Address as a string
  def self.get_company_address( company_id )
    company = self.find(company_id)
    address = ""
    company.house_number.blank? ? address : address << "#{company.house_number}"
    company.street.blank? ? address : address << ", #{company.street}"
    company.colony.blank? ? address : address << ", #{company.colony}"
    company.landmark.blank? ? address : address << ", #{company.landmark}"
    company.city.blank? ? address : address << ", #{company.city}"
    #company.state.blank? ? address : address << ", #{company.state}"
    #company.zipcode.blank? ? address : address << "- #{company.zipcode}"
    address.blank? ? "-" : address
  end


  ### Get Contact Numbers as a string
  def self.get_company_contacts( company )
    contacts = ""
    company.mobile_number.blank? ? contacts : contacts << "#{(company.mobile_number)}"
    company.phone_number.blank? ? contacts : contacts << ", #{(company.phone_number)}"
    contacts.blank? ? "-" : contacts
  end

end
