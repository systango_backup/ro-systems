class Assembly < ActiveRecord::Base
require 'aasm'
  has_many :manifests
  has_many :products, :through => :manifests

  validates :name, :presence => true
  validates :description, :presence => true
  validates :quantity, :presence => true, :numericality => true

### Begin AASM definitions
  
  include AASM
 
  aasm_column :current_state
  
  aasm_initial_state :assemble
  
  aasm_state :assemble
  aasm_state :sold, :after_enter => :reduce_product

  aasm_event :assemble_to_sold do
    transitions :to => :assemble, :from => [:sold]
  end

  def reduce_product
  end

end
